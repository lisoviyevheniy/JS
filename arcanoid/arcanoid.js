(function ($) {
    var context, canvas, gWidth, gHeight, flag = 1;
    var toRight = false, toLeft = false;
    var ball, platform, blocks;
    function init(cxt, cnvs, thisWidth, thisHeight) {
        document.addEventListener('keydown', function (event) {
            switch (event.keyCode) {
                case 37:
                    toLeft = true;
                    toRight = false;
                    break;
                case 39:
                    toLeft = false;
                    toRight = true;
                    break;
            }
        });
        document.addEventListener('keyup', function (event) {
            switch (event.keyCode) {
                case 37:
                case 39:
                    toLeft = false;
                    toRight = false;
                    break;
            }
        });
        context = cxt;
        canvas = cnvs;
        gWidth = thisWidth;
        gHeight = thisHeight;
        ball = new Ball(gWidth / 2, gHeight / 2);
        platform = new Platform(gWidth / 2 - 50, gHeight - 30);
        blocks = new Blocks(gWidth / 20, 20, 13, 6);
        window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        start();
    }
    function start() {
        if (flag) {
            context.clearRect(0, 0, gWidth, gHeight);
            ball.x += ball.SpeedX;
            ball.y += ball.SpeedY;
            //BALL
            if ((ball.x + ball.R + ball.SpeedX > gWidth) || (ball.x - ball.R + ball.SpeedX < 0)) {
                ball.SpeedX = -1 * ball.SpeedX;
            } else if (ball.y - ball.R + ball.SpeedY < 0) {
                ball.SpeedY = -1 * ball.SpeedY;
            } else if (ball.y + ball.R + ball.SpeedY > gHeight) {
                flag = 0;
            }
            //Platform
            if (toLeft && platform.x - platform.SpeedX > 0) {
                platform.x -= platform.SpeedX;
            }
            if (toRight && platform.x + platform.width + platform.SpeedX < gWidth) {
                platform.x += platform.SpeedX;
            }
            //HIT BY PLATFORM
            if ((ball.x + ball.R + ball.SpeedX > platform.x && ball.x - ball.R + ball.SpeedX < platform.x + platform.width &&
                ball.y + ball.R + ball.SpeedY > platform.y && ball.y + ball.R + ball.SpeedY < platform.y + platform.height) ||
                (ball.x - ball.R + ball.SpeedX > platform.x && ball.x - ball.R + ball.SpeedX < platform.x + platform.width &&
                ball.y + ball.R + ball.SpeedY > platform.y && ball.y + ball.R + ball.SpeedY < platform.y + platform.height)) {
                ball.SpeedY = -1 * ball.SpeedY;
                if (toLeft && Math.abs(ball.SpeedY) < 10) {
                    ball.SpeedX = 0.02 * ball.SpeedX + ball.SpeedX;
                    if (ball.SpeedX > 0) {
                        ball.SpeedY += 0.5;
                    } else ball.SpeedY -= 0.5;
                } else if (toRight && Math.abs(ball.SpeedY) < 10) {
                    ball.SpeedX = 0.02 * ball.SpeedX + ball.SpeedX;
                    if (ball.SpeedX < 0) {
                        ball.SpeedY += 0.5;
                    } else ball.SpeedY -= 0.5;
                }
            }
            //HIT By ANGLE OF PLATFORM
            if ((ball.x + ball.R + ball.SpeedX == platform.x || ball.x - ball.R + ball.SpeedX == platform.x + platform.width) &&
                ball.y + ball.R + ball.SpeedY < platform.y + platform.height && ball.y + ball.R + ball.SpeedY > platform.y) {
                ball.SpeedX = -1 * ball.SpeedX;
                ball.SpeedY = -1 * ball.SpeedY;
            }
            //HIT BY BLOCK
            for (var g = 0; g < blocks.M; g++) {
                for (var k = 0; k < blocks.N; k++) {
                    var row = Math.floor(ball.y / blocks.M * (blocks.margin + blocks.height));
                    var col = Math.floor(ball.x / blocks.N * (blocks.width + blocks.margin));
                    if (blocks.arr[g][k][0] == 1) {
                        if (g * (blocks.margin + blocks.height) < ball.y - ball.R + ball.SpeedY &&
                            (g + 1) * (blocks.margin + blocks.height) - blocks.margin > ball.y - ball.R + ball.SpeedY &&
                            k * (blocks.margin + blocks.width) < ball.x &&
                            (k + 1) * (blocks.margin + blocks.width) > ball.x && row > 0 && col > 0) {
                            if (blocks.arr[g][k][1] == 1) {
                                blocks.arr[g][k][0] = 0;
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            } else if (blocks.arr[g][k][1] == 2) {
                                blocks.arr[g][k][1] = 1;
                                context.fillStyle = '#A74600';
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            } else {
                                blocks.arr[g][k][1] = 2;
                                context.fillStyle = '#A79827';
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            }
                            ball.SpeedY = -1 * ball.SpeedY;
                        }
                        if (g * (blocks.margin + blocks.height) > ball.y - ball.R + ball.SpeedY &&
                            (g + 1) * (blocks.margin + blocks.height) - blocks.margin > ball.y - ball.R + ball.SpeedY &&
                            ((k * (blocks.margin + blocks.width) < ball.x + ball.R + ball.SpeedX &&
                            (k - 1) * (blocks.margin + blocks.width) > ball.x - ball.R - ball.SpeedX) ||
                            ((k + 1) * (blocks.margin + blocks.width) > ball.x - ball.R + ball.SpeedX &&
                            (k + 2) * (blocks.margin + blocks.width) < ball.x + ball.R - ball.SpeedX))
                            && row > 0 && col > 0) {
                            if (blocks.arr[g][k][1] == 1) {
                                blocks.arr[g][k][0] = 0;
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            } else if (blocks.arr[g][k][1] == 2) {
                                blocks.arr[g][k][1] = 1;
                                context.fillStyle = '#A74600';
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            } else {
                                blocks.arr[g][k][1] = 2;
                                context.fillStyle = '#A79827';
                                context.beginPath();
                                context.fillRect(k * (blocks.width + blocks.margin), g * (blocks.height + blocks.margin), blocks.width, blocks.height);
                                context.closePath();
                            }
                            ball.SpeedX = -1 * ball.SpeedX;
                        }
                    }
                }
            }
            //BALL
            context.fillStyle = ball.color;
            context.beginPath();
            context.arc(ball.x, ball.y, ball.R, 0, 2 * Math.PI, true);
            context.closePath();
            context.fill();
            //Platform
            context.fillStyle = platform.color;
            context.beginPath();
            context.fillRect(platform.x, platform.y, platform.width, platform.height);
            //Blocks
            for (var i = 0; i < blocks.M; i++) {
                for (var j = 0; j < blocks.N; j++) {
                    if (blocks.arr[i][j][1] == 1) {
                        context.fillStyle = '#A74600';
                    } else if (blocks.arr[i][j][1] == 2) {
                        context.fillStyle = '#A79827';
                    } else context.fillStyle = '#A79C8C';
                    if (blocks.arr[i][j][0] == 1) {
                        context.beginPath();
                        context.fillRect(j * (blocks.width + blocks.margin), i * (blocks.height + blocks.margin), blocks.width, blocks.height);
                        context.closePath();
                    }
                }
            }
            //Call recursive animation
            window.requestAnimationFrame(start);
        } else {
            context.fillStyle = '#1E366D';
            context.fillRect(0, 0, gWidth, gHeight);
            context.fillStyle = "#8CA76C";
            context.strokeStyle = "#8CA76C";
            context.font = "italic 30pt Arial";
            context.fillText("Game over!!!!", gWidth/2 - 110, gHeight/2);
        }
    }

    var Ball = function (x, y) {
        this.x = x;
        this.y = y;
        this.color = 'red';
        this.R = 10;
        this.SpeedX = 3;
        this.SpeedY = -5;
    };
    var Platform = function (x, y) {
        this.x = x;
        this.y = y;
        this.color = 'black';
        this.height = 10;
        this.width = 100;
        this.SpeedX = 7;
    };
    var Blocks = function (x, y, N, M) {
        this.N = N;
        this.M = M;
        this.x = x;
        this.y = y;
        this.color = 'black';
        this.height = 20;
        this.width = 50;
        this.margin = 10;
        this.arr = [];
        for (var i = 0; i < M; i++) {
            this.arr[i] = [];
            for (var j = 0; j < N; j++) {
                this.arr[i][j] = [1, Math.floor(1 + (Math.random() * 3))]
            }
        }
    };
    $.fn.arcanoid = function (method) {
        var $this = this;
        switch (method) {
            case 'init':
                init($this.get(0).getContext('2d'), $this, $this.get(0).width, $this.get(0).height);
                break;
        }
    }
}(jQuery));
