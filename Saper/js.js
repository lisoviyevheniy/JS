function GenTable(n, m) {
    var obj = this;
    this.nOfBomb = prompt('Number of bombs (more than 10 and less than ' + (n * m - 1) + '):', 12);
    this.nOfBomb = this.nOfBomb > 10 ? this.nOfBomb : 10;
    if (this.nOfBomb > n * m - 1)
        this.nOfBomb = n * m - 1;
    this.matr = [];
    for (var i = 0; i < n; i++) {
        this.matr[i] = [];
        for (var j = 0; j < m; j++)
            this.matr[i][j] = '';
    }
    this.rand = function () {
        for (i = 0; i < this.nOfBomb; i++) {
            var I = Math.floor(Math.random() * n);
            var J = Math.floor(Math.random() * m);
            if (this.matr[I][J] == 0) {
                this.matr[I][J] = 'bomb';
            }
            else {
                i--;
            }
        }
        for (i = 0; i < n; i++) {
            for (j = 0; j < m; j++) {
                if (this.matr[i][j] == 'bomb') {
                    var maxI = i + 1 >= n ? i : i + 1;
                    var maxJ = j + 1 >= m ? j : j + 1;
                    for (var z = i - 1; z <= maxI; z++) {
                        if (z < 0)
                            z++;
                        for (var g = j - 1; g <= maxJ; g++) {
                            if (g < 0)
                                g++;
                            if (this.matr[z][g] != 'bomb')
                                this.matr[z][g]++;
                        }
                    }
                }
            }
        }
    };
    this.generating = function () {
        counter = n * m - this.nOfBomb;
        this.rand();
        this.table = document.createElement('table');
        this.table.id = 'wrapper';
        var tr = '';
        for (var i = 0; i < n; i++) {
            tr += '<tr>';
            var td = '';
            for (var j = 0; j < m; j++) {
                if (this.matr[i][j] == 'bomb')
                    td += '<td width="28px" height="28px" class="hide" data-value="bomb" id="' + i + ',' + j +
                        '">&#9967;</td>';
                else td += '<td width="28px" height="28px" class="hide" id="' + i + ',' + j + '" data-value="' +
                    this.matr[i][j] + '">' + this.matr[i][j] + '</td>';
            }
            tr += td + '</tr>';
            this.table.innerHTML = tr;
        }
        this.table.setAttribute('border', '1px');
        this.table.onclick = function (event) {
            var elem = event.target;
            if (elem.tagName != 'TABLE' && elem.tagName != 'TBODY' && elem.tagName != 'TR') {
                if (elem.className == 'hide' || elem.className == 'flag') {
		    var audio = new Audio(); 
    		    audio.src = 'open.mp3'; 
    		    audio.autoplay = true; 
                    elem.className = 'white';
                    if (elem.getAttribute('data-value') == 'bomb')
                        obj.winOrLose(0);
                    else if (elem.getAttribute('data-value') == '') {
                        var id = elem.getAttribute('id');
                        var I = id.substr(0, id.length - id.indexOf(',') - 1);
                        var J = id.substr(id.length - id.indexOf(','));
                        obj.unlocking(I, J, n, m);
                    }
                    if (elem.getAttribute('data-value') != 'bomb') {
                        counter--;
                    }
                    if (counter == 0)
                        obj.winOrLose(1);
                }
            }
        };
        this.table.oncontextmenu = function (event) {
            var elem = event.target;
            if (elem.tagName != 'TABLE' && elem.tagName != 'TBODY' && elem.tagName != 'TR') {
                if (elem.className == 'hide') {
                    elem.className = 'flag';
                }
                else if (elem.className == 'flag') {
                    elem.className = 'hide';
                }
            }
            return false;
        };
        document.body.appendChild(this.table);
    };
    this.winOrLose = function (win) {
        var block = document.getElementById('template');
        var popup = document.createElement('div');
        popup.className = 'question';
        if (win == 1) {
            soundWin();
            popup.innerHTML = block.innerHTML.replace("{{text}}", "You win");
        }
        else {
            soundLose();
            popup.innerHTML = block.innerHTML.replace('{{text}}', 'You lose');
        }
        document.body.appendChild(popup);
    };
    this.unlocking = function (i, j, n, m) {
        i = parseInt(i);
        j = parseInt(j);
        if (i - 1 >= 0) {
            for (var k = j - 1; k <= j + 1; k++) {
                if (k < 0)
                    continue;
                if (k >= m)
                    continue;
                var td = document.getElementById((i - 1) + ',' + k);
                if (td.getAttribute('data-value') == '' && td.className != 'white') {
                    td.className = 'white';
                    counter--;
                    obj.unlocking(i - 1, k, n, m);
                }
                else if (td.getAttribute('data-value') != 'bomb' && td.className != 'white') {
                    td.className = 'white';
                    counter--;
                }
            }
        }
        for (k = j - 1; k <= j + 1; k++) {
            if (k < 0)
                continue;
            if (k >= m)
                continue;
            td = document.getElementById(i + ',' + k);
            if (td.getAttribute('data-value') == '' && td.className != 'white') {
                td.className = 'white';
                counter--;
                obj.unlocking(i, k, n, m);
            }
            else if (td.getAttribute('data-value') != 'bomb' && td.className != 'white') {
                td.className = 'white';
                counter--;
            }
        }
        if (i + 1 < n) {
            for (k = j - 1; k <= j + 1; k++) {
                if (k < 0)
                    continue;
                if (k >= m)
                    continue;
                td = document.getElementById((i + 1) + ',' + k);
                if (td.getAttribute('data-value') == '' && td.className != 'white') {
                    td.className = 'white';
                    counter--;
                    obj.unlocking(i + 1, k, n, m);
                }
                else if (td.getAttribute('data-value') != 'bomb' && td.className != 'white') {
                    td.className = 'white';
                    counter--;
                }
            }
        }
    };
}
function refresh() {
    location.reload();
}
function soundLose() {
    var audio = new Audio(); 
    audio.src = 'lose.mp3'; 
    audio.autoplay = true;
}
function soundWin() {
    var audio = new Audio(); 
    audio.src = 'win.mp3'; 
    audio.autoplay = true; 
}
var flag = 1;
var counter = -1;
var N = prompt('Write a height of fied:', 10);
var M = prompt('Write a width of fied:', 10);
var tab = new GenTable(N > 10 ? N : 10, M > 10 ? M : 10);
tab.generating();