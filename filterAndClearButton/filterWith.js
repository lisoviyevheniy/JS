(function ($) {
    /**
     * @param $table jQuery
     * @param regexp RegExp
     */
    var filter = function ($table, regexp) {
        var $elements = $table.find('tr, li').not('thead tr, tfoot tr, th');
        $elements.each(function () {
            var $this = $(this);
            if (!regexp.test($this.text()))
                $this.hide();
            else $this.show();
        })
    };
    /**
     * @param text String
     */
    var makeRegExp = function (text) {
        var words = text.split(/\s/);
        var regexp = '';
        if (words.length > 1) {
            for (var i in words) {
                regexp += words[i];
                regexp += '[\\W\\s]*'
            }
        }
        else regexp = text;
        return new RegExp(regexp, 'img')
    };

    $.fn.filterWith = function (method, op) {
        var $this = $(this);
        var $input = $('');
        makeRegExp('Hello world');
        switch (method) {
            case 'assignInput':
                $input = $(op);
                if (!$input.length)throw new Error('Element not found');
                $input.on('input', function (event) {
                    var $el = $(event.target);
                    filter($this, makeRegExp($el.val()));
                });
                break;
            case 'set' :
                if (op == undefined) throw new Error('Missing second parameter');
                filter(this, makeRegExp(op));
        }
    }
}(jQuery));
