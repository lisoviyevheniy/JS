(function ($) {
    var XClickHandler = function (event) {
        var $this = $(event.target);
        $this.prev().val('')
            .trigger('focus')
            .trigger('input')
            .trigger('change');
        $this.hide();
    };

    var inputChangeHandler = function (event) {
        var $this = $(event.target);
        if ($this.val())
            $this.next().filter('.X').show();
        else $this.next().filter('.X').hide();
    };

    $.fn.addClearButton = function () {
        var $this = $(this);
        $this.css({paddingRight: "1.6%"})
            .after($('<span class="X">❌</span>')
                .css({
                    fontSize: $this.css('height') - $this.css('padding') + 'px',
                    display: "none"
                })
                .on('click', XClickHandler)
            )
            .on('input', inputChangeHandler);
        if ($this.val())
            $this.next().filter('.X').show();
        return this;
    };

}(jQuery));
